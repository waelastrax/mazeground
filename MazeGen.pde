import java.util.ArrayList;

int next = 0;
int cols, rows;
int cellSize = 10;
boolean generated = false;
ArrayList<Cell> maze = new ArrayList<Cell>();
ArrayList<Integer> stack = new ArrayList<Integer>();

void setup ()
{
  size (400, 400, JAVA2D);
  frameRate(30);
  orientation(LANDSCAPE);
  cols = floor(width/cellSize);
  rows = floor(height/cellSize);
  initMaze();
  maze.get(0).setVisited(true);
  stack.add(0);
}

void initMaze()
{
  for (int y = 0; y < rows; y ++)
  {
    for (int x = 0; x < cols; x ++)
    {
      maze.add(new Cell(x, y));
    }
  }
}

int getNext(int x, int y)
{ 
  //top, righ, bottom, left
  ArrayList<Integer> neighbors = new ArrayList<Integer>();
  if(index(x, y - 1) > -1 && !maze.get(index(x, y - 1)).isVisited())
  {
    neighbors.add(index(x, y - 1));
  }
  if(index(x + 1, y) > -1 && !maze.get(index(x + 1, y)).isVisited())
  {
    neighbors.add(index(x + 1, y));
  }
  if(index(x, y + 1) > -1 && !maze.get(index(x, y + 1)).isVisited())
  {
    neighbors.add(index(x, y + 1));
  }
  if(index(x - 1, y) > -1 && !maze.get(index(x - 1, y)).isVisited())
  {
    neighbors.add(index(x - 1, y));
  }
  if(neighbors.size() > 0)
  {
    return neighbors.get((int)random(0, neighbors.size()));
  }
  else
  {
    return -1;
  }
}

int index(int x, int y)
{
  if(x < 0 || y < 0 || x > cols - 1 || y > rows - 1)
  {
    return -1;
  }
  return x + y * cols;
}

void draw()
{
  background(0);
  drawMaze();
}

void drawMaze()
{
  fill(0);
  for (int i = 0; i < maze.size(); i ++)
  {
    if(maze.get(i).isVisited())
    {
      fill(255, 0, 255, 100);
      noStroke();
      rect(maze.get(i).getX() * cellSize, maze.get(i).getY() * cellSize, cellSize, cellSize);
    }
    
    stroke(255);
    if (maze.get(i).getWall(0))
    {
      line(maze.get(i).getX() * cellSize, maze.get(i).getY() * cellSize, 
          (maze.get(i).getX() + 1) * cellSize, maze.get(i).getY() * cellSize);
    }
    if (maze.get(i).getWall(1))
    {
      line((maze.get(i).getX() + 1) * cellSize, maze.get(i).getY() * cellSize, 
          (maze.get(i).getX() + 1) * cellSize, (maze.get(i).getY() + 1) * cellSize);
    }
    
    if (maze.get(i).getWall(2))
    {
      line((maze.get(i).getX() + 1) * cellSize, (maze.get(i).getY() + 1) * cellSize, 
          (maze.get(i).getX()) * cellSize, (maze.get(i).getY() + 1) * cellSize);
    }
    
    if (maze.get(i).getWall(3))
    {
      line(maze.get(i).getX() * cellSize, (maze.get(i).getY() + 1) * cellSize, 
          maze.get(i).getX() * cellSize, maze.get(i).getY() * cellSize);
    }
  }
  
  if (!generated)
  {
    generate();
  }
}

void generate()
{
  if(stack.size() > 0)
  {
    fill(0, 255, 0);
    rect(maze.get(stack.get(stack.size() - 1)).getX() * cellSize, maze.get(stack.get(stack.size() - 1)).getY() * cellSize, cellSize, cellSize);
    next = getNext(maze.get(stack.get(stack.size() - 1)).getX(), maze.get(stack.get(stack.size() - 1)).getY());
    if(next > -1) 
    {
      removeWalls(stack.get(stack.size() - 1), next);
      maze.get(next).setVisited(true);
      stack.add(new Integer(next));
    }
    else
    {
      stack.remove(stack.size() - 1);
    }
  }
  else
  {
    generated = true;
  }
}

void removeWalls(int c, int n)
{
  int diff = n - c;
  if (diff == 1)
  {
    maze.get(c).setWall(1,false);
    maze.get(n).setWall(3,false);
  }
  if (diff == -1)
  {
    maze.get(c).setWall(3,false);
    maze.get(n).setWall(1,false);
  }
  if (diff == -cols)
  {
    maze.get(c).setWall(0,false);
    maze.get(n).setWall(2,false);
  }
  if (diff == cols)
  {
    maze.get(c).setWall(2,false);
    maze.get(n).setWall(0,false);  
  }
}
