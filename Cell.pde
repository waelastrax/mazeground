public class Cell
{
  boolean visited;
  int x, y;
  boolean [] walls = {true, true, true, true}; //top right, bottom, left
  
  public Cell(int x, int y)
  {
    this.visited = false;
    this.x = x;
    this.y = y;
  }
  
  public void setVisited(boolean vis)
  {
    this.visited = vis;
  }
  
  public boolean isVisited()
  {
    return visited;
  }
  
  public void setWall(int i, boolean wall)
  {
    this.walls[i] = wall;
  }
  
  public boolean getWall(int i)
  {
    return walls[i];
  }
  
  public int getX()
  {
    return x;
  }
  
  public int getY()
  {
    return y;
  }
}
